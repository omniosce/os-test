#ifdef __HAIKU__
#define _BSD_SOURCE
#endif

#include <sys/socket.h>

#if defined(__FreeBSD__) || defined(__NetBSD__) || defined(__DragonFly__) || defined(__minix__)
#include <sys/endian.h>
#elif defined(__APPLE__)
#define htobe16 htons
#define htobe32 htonl
#else
#include <endian.h>
#endif
/*#include <err.h>*/
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/in.h>
#include <poll.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

// Address to send packets too that do not send back any ICMP connection refused
// packets.
#define BLACKHOLE_HOST 0x08080808
#define BLACKHOLE_PORT 53

const char* strerrno(int errnum)
{
	switch ( errnum )
	{
	case 0: return "errno == 0";
#if EOPNOTSUPP != ENOTSUP
	case EOPNOTSUPP: return "ENOTSUP";
#endif
#if EWOULDBLOCK != EAGAIN
	case EAGAIN: return "EAGAIN";
#endif
#ifdef EPFNOSUPPORT
	case EPFNOSUPPORT: return "EPFNOSUPPORT";
#endif
	case EACCES: return "EACCES";
	case EADDRINUSE: return "EADDRINUSE";
	case EADDRNOTAVAIL: return "EADDRNOTAVAIL";
	case EAFNOSUPPORT: return "EAFNOSUPPORT";
	case ECONNABORTED: return "ECONNABORTED";
	case ECONNREFUSED: return "ECONNREFUSED";
	case EDESTADDRREQ: return "EDESTADDRREQ";
	case EDOM: return "EDOM";
	case EFAULT: return "EFAULT";
	case EFBIG: return "EFBIG";
	case EHOSTUNREACH: return "EHOSTUNREACH";
	case EINPROGRESS: return "EINPROGRESS";
	case EINVAL: return "EINVAL";
	case EISCONN: return "EISCONN";
	case EMSGSIZE: return "EMSGSIZE";
	case ENETRESET: return "ENETRESET";
	case ENETUNREACH: return "ENETUNREACH";
	case ENOBUFS: return "ENOBUFS";
	case ENODEV: return "ENODEV";
	case ENOMEM: return "ENOMEM";
	case ENOPROTOOPT: return "ENOPROTOOPT";
	case ENOSPC: return "ENOSPC";
	case ENOSYS: return "ENOSYS";
	case ENOTCONN: return "ENOTCONN";
	case ENOTSOCK: return "ENOTSOCK";
	case ENOTSUP: return "ENOTSUP";
	case EOVERFLOW: return "EOVERFLOW";
	case EPERM: return "EPERM";
	case EPIPE: return "EPIPE";
	case EPROTONOSUPPORT: return "EPROTONOSUPPORT";
	case EPROTO: return "EPROTO";
	case EPROTOTYPE: return "EPROTOTYPE";
	case ERANGE: return "ERANGE";
	case ESPIPE: return "ESPIPE";
	case ETIMEDOUT: return "ETIMEDOUT";
	case EWOULDBLOCK: return "EWOULDBLOCK";
	default: return strerror(errnum);
	}
}

__attribute__((unused))
static void sigpipe(int signum)
{
	(void) signum;
	printf("SIGPIPE\n");
}

__attribute__((unused))
static void test_vwarnc(int errnum, const char* fmt, va_list ap)
{
	if ( fmt )
	{
		vfprintf(stderr, fmt, ap);
		fputs(": ", stderr);
	}
	fprintf(stderr, "%s\n", strerrno(errnum));
}

__attribute__((unused))
static void test_vwarn(const char* fmt, va_list ap)
{
	test_vwarnc(errno, fmt, ap);
}

__attribute__((unused))
static void test_warn(const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	test_vwarn(fmt, ap);
	va_end(ap);
}

__attribute__((unused))
static void test_vwarnx(const char* fmt, va_list ap)
{
	if ( fmt )
		vfprintf(stderr, fmt, ap);
	fputc('\n', stderr);
}

__attribute__((unused))
static void test_warnx(const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	test_vwarnx(fmt, ap);
	va_end(ap);
}

__attribute__((unused))
static void test_verr(int exitcode, const char* fmt, va_list ap)
{
	test_vwarn(fmt, ap);
	exit(exitcode);
}

__attribute__((unused))
static void test_err(int exitcode, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	test_verr(exitcode, fmt, ap);
	va_end(ap);
}

__attribute__((unused))
static void test_verrx(int exitcode, const char* fmt, va_list ap)
{
	test_vwarnx(fmt, ap);
	exit(exitcode);
}

__attribute__((unused))
static void test_errx(int exitcode, const char* fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	test_verrx(exitcode, fmt, ap);
	va_end(ap);
}

#define err test_err
#define errc test_errc
#define errx test_errx
#define verr test_err
#define verrc test_errc
#define verrx test_errx
#define warn test_warn
#define warnc test_warnc
#define warnx test_warnx
#define vwarn test_warn
#define vwarnc test_warnc
#define vwarnx test_warnx
