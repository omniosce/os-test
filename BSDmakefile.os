OS_LIST != ls os
SUITE_LIST != cat misc/suites.list

.PHONY: all
all: test

.PHONY: test
test: $(OS_LIST)

.for OS in $(OS_LIST)
.PHONY: $(OS)
$(OS):
	mkdir -p out
	rm -rf 'out/$(OS)'
	mkdir -p 'out/$(OS)'
	cp -R -t 'out/$(OS)' -- Makefile BSDmakefile GNUmakefile misc $(SUITE_LIST)
	cd 'out/$(OS)' && '../../os/$(OS)'

$(OS)-clean: os/$(OS)
	rm -rf 'out/$(OS)'
.endfor

clean:
	rm -rf out
	rm -f os-test.html

.PHONY: html
html: os-test.html

.PHONY: os-test.html
os-test.html: test
	misc/html.sh --enable-legend --enable-suites-overview --os-list "$(OS_LIST)" --suite-list "$(SUITE_LIST)" > os-test.html
